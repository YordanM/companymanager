package com.internship.dao;

import com.internship.constant.Constants;
import com.internship.pojo.Company;
import com.internship.pojo.Representative;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;

/**
 * @author Yordan Marinov
 */
@Repository
public class CompanyDaoImpl implements Dao<Company> {

    private final static Logger logger = LoggerFactory.getLogger(CompanyDaoImpl.class);
    private JdbcTemplate connection;

    public CompanyDaoImpl(JdbcTemplate connection) {
        this.connection = connection;
    }

    @Override
    public List<Company> getAll() {
        try {
            return connection.query(Constants.sqlGetAllCompanies, (rs, rowNum) ->
                    new Company(
                            rs.getInt("company_id"),
                            rs.getString("name"),
                            rs.getString("town"),
                            rs.getString("phone_number"),
                            rs.getInt("founding_year"),
                            rs.getInt("representative_id")
                    )
            );
        } catch (EmptyResultDataAccessException emptyData) {
            logger.error("No results returned from query.");
        }
        return null;

    }

    @Override
    public Company getById(int id) {
        try {
            return connection.queryForObject(Constants.sqlGetCompanyById, new Object[]{id}, (rs, rowNum) ->
                    new Company(
                            rs.getInt("company_id"),
                            rs.getString("name"),
                            rs.getString("town"),
                            rs.getString("phone_number"),
                            rs.getInt("founding_year"),
                            rs.getInt("representative_id")
                    ));
        } catch (EmptyResultDataAccessException emptyData) {
            logger.error("No result returned from query.");
        }
        return null;
    }

    private boolean representativeExists(int id) {
        try {
            connection.queryForObject(Constants.sqlGetRepresentativeById, new Object[]{id}, (rs, rowNum) ->
                    new Representative(
                            rs.getInt("representative_id"),
                            rs.getString("first_name"),
                            rs.getString("middle_name"),
                            rs.getString("last_name"),
                            rs.getString("representative_phone_number"),
                            rs.getString("email_address")
                    ));
            return true;
        } catch (EmptyResultDataAccessException emptyData) {
            return false;
        }
    }

    @Override
    public int save(Company toSave) {
        Object[] parameters = new Object[]{
                toSave.getName(),
                toSave.getTown(),
                toSave.getPhoneNumber(),
                toSave.getFoundingYear(),
                toSave.getRepresentativeID(),
                toSave.getCompanyID()
        };
        if (!representativeExists(toSave.getRepresentativeID())) {
            parameters[4] = null;
        }
        int status = connection.update(Constants.sqlSaveCompany,
                parameters, new int[]{
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.INTEGER,
                        Types.INTEGER,
                        Types.INTEGER
                });
        logger.info(status != 0 ? "Successfully saved to database." : "Error while saving to database.");
        return status;
    }

    @Override
    public int update(Company toUpdate) {
        int status = connection.update(Constants.sqlUpdateCompany, new Object[]{
                toUpdate.getName(),
                toUpdate.getTown(),
                toUpdate.getPhoneNumber(),
                toUpdate.getFoundingYear(),
                toUpdate.getRepresentativeID(),
                toUpdate.getCompanyID()
        }, new int[]{
                Types.VARCHAR,
                Types.VARCHAR,
                Types.VARCHAR,
                Types.INTEGER,
                Types.INTEGER,
                Types.INTEGER
        });
        logger.info(status != 0 ? "Successfully updated in database." : "Error while updating in database.");
        return status;
    }

    @Override
    public int delete(int id) {
        int status = connection.update(Constants.sqlDeleteCompany, id);
        logger.info(status != 0 ? "Successfully deleted from database." : "Error while deleting from database.");
        return status;
    }
}
