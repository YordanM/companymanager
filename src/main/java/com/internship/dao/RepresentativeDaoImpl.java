package com.internship.dao;

import com.internship.constant.Constants;
import com.internship.pojo.Representative;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;

/**
 * @author Yordan Marinov
 */
@Repository
public class RepresentativeDaoImpl implements Dao<Representative> {

    private final static Logger logger = LoggerFactory.getLogger(CompanyDaoImpl.class);
    private JdbcTemplate connection;

    public RepresentativeDaoImpl(JdbcTemplate connection) {
        this.connection = connection;
    }

    @Override
    public List<Representative> getAll() {
        try {
            return connection.query(Constants.sqlGetAllRepresentatives, (rs, rowNum) ->
                    new Representative(
                            rs.getInt("representative_id"),
                            rs.getString("first_name"),
                            rs.getString("middle_name"),
                            rs.getString("last_name"),
                            rs.getString("representative_phone_number"),
                            rs.getString("email_address")
                    )
            );
        } catch (
                EmptyResultDataAccessException emptyData) {
            logger.error("No results returned from query.");
        }
        return null;
    }

    @Override
    public Representative getById(int id) {
        try {
            return connection.queryForObject(Constants.sqlGetRepresentativeById, new Object[]{id}, (rs, rowNum) ->
                    new Representative(
                            rs.getInt("representative_id"),
                            rs.getString("first_name"),
                            rs.getString("middle_name"),
                            rs.getString("last_name"),
                            rs.getString("representative_phone_number"),
                            rs.getString("email_address")
                    ));
        } catch (EmptyResultDataAccessException emptyData) {
            logger.error("No result returned from query.");
        }
        return null;
    }

    @Override
    public int save(Representative toSave) {
        int status = connection.update(Constants.sqlSaveRepresentative,
                new Object[]{
                        toSave.getFirstName(),
                        toSave.getMiddleName(),
                        toSave.getLastName(),
                        toSave.getPhoneNumber(),
                        toSave.getEmailAddress(),
                        toSave.getRepresentativeID()
                }, new int[]{
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.INTEGER
                });
        logger.info(status != 0 ? "Successfully saved to database." : "Error while saving to database.");
        return status;
    }

    @Override
    public int update(Representative toUpdate) {
        int status = connection.update(Constants.sqlUpdateRepresentative,
                new Object[]{
                        toUpdate.getFirstName(),
                        toUpdate.getMiddleName(),
                        toUpdate.getLastName(),
                        toUpdate.getPhoneNumber(),
                        toUpdate.getEmailAddress(),
                        toUpdate.getRepresentativeID()
                }, new int[]{
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.VARCHAR,
                        Types.INTEGER
                });
        logger.info(status != 0 ? "Successfully updated in database." : "Error while updating in database.");
        return status;
    }

    @Override
    public int delete(int id) {
        int status = connection.update(Constants.sqlDeleteRepresentative, id);
        logger.info(status != 0 ? "Successfully deleted from database." : "Error while deleting from database.");
        return status;
    }
}
