package com.internship.dao;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public interface Dao<E> {
    /**
     * @return Returns a list of objects from a specific type
     */
    List<E> getAll();

    /**
     * @param id The id of the entity inside the database to be fetched
     * @return An object from a specific type
     */
    E getById(int id);

    /**
     * @param toSave An object from a specific type to be saved to the database
     * @return Returns 0 if no changes were made to the database, else returns the number of rows affected
     */
    int save(E toSave);

    /**
     * @param toUpdate An object from a specific type to be updated in the database
     * @return Returns 0 if no changes were made to the database, else returns the number of rows affected
     */
    int update(E toUpdate);

    /**
     * @param id The id of the entity inside the database to be deleted
     * @return Returns 0 if no changes were made to the database, else returns the number of rows affected
     */
    int delete(int id);
}
