package com.internship.service;

import com.internship.dao.Dao;
import com.internship.pojo.Company;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Yordan Marinov
 */

@Service
public class CompanyServiceImpl implements IService<Company> {
    private Dao<Company> dao;

    CompanyServiceImpl(Dao<Company> dao) {
        this.dao = dao;
    }

    @Override
    public List<Company> getAllEntities() {
        return dao.getAll();
    }

    @Override
    public Company getEntityById(int id) {
        return dao.getById(id);
    }

    @Override
    public int saveEntity(Company toSave) {
        return dao.save(toSave);
    }

    @Override
    public int updateEntity(Company toUpdate) {
        return dao.update(toUpdate);
    }

    @Override
    public int deleteEntity(int id) {
        return dao.delete(id);
    }
}
