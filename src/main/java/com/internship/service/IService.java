package com.internship.service;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public interface IService<E> {
    /**
     * @return Returns a list of objects from a specific type
     */
    List<E> getAllEntities();

    /**
     * @param id The id of the entity inside the database to be fetched
     * @return An object from a specific type
     */
    E getEntityById(int id);

    /**
     * @param toSave An object from a specific type to be saved to the database
     * @return Returns 0 if no changes were made to the database, else returns the number of rows affected
     */
    int saveEntity(E toSave);

    /**
     * @param toUpdate An object from a specific type to be updated in the database
     * @return Returns 0 if no changes were made to the database, else returns the number of rows affected
     */
    int updateEntity(E toUpdate);

    /**
     * @param id The id of the entity inside the database to be deleted
     * @return Returns 0 if no changes were made to the database, else returns the number of rows affected
     */
    int deleteEntity(int id);
}
