package com.internship.service;

import com.internship.dao.Dao;
import com.internship.pojo.Representative;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Yordan Marinov
 */
@Repository
public class RepresentativeServiceImpl implements IService<Representative> {
    private Dao<Representative> dao;

    public RepresentativeServiceImpl(Dao<Representative> dao) {
        this.dao = dao;
    }

    @Override
    public List<Representative> getAllEntities() {
        return dao.getAll();
    }

    @Override
    public Representative getEntityById(int id) {
        return dao.getById(id);
    }

    @Override
    public int saveEntity(Representative toSave) {
        return dao.save(toSave);
    }

    @Override
    public int updateEntity(Representative toUpdate) {
        return dao.update(toUpdate);
    }

    @Override
    public int deleteEntity(int id) {
        return dao.delete(id);
    }
}
