package com.internship.controller;

import com.internship.pojo.Company;
import com.internship.service.CompanyServiceImpl;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Yordan Marinov
 */
@RestController
public class CompanyController {
    private CompanyServiceImpl service;

    public CompanyController(CompanyServiceImpl service) {
        this.service = service;
    }

    /**
     * Creates an endpoint '/companies', at which all of the companies are displayed
     *
     * @return Returns a list of companies, retrieved from the database
     */
    @GetMapping(value = "/companies")
    public List<Company> getAll() {
        return service.getAllEntities();
    }

    /**
     * Creates an endpoint '/companies/get', at which the company, referred by the id, is displayed
     *
     * @param id The id of the company to be fetched from the database
     * @return Returns the company which has the specified id, or null if no company with that id exists
     */
    @GetMapping(value = "/companies/get")
    public Company getCompany(@RequestParam int id) {
        return service.getEntityById(id);
    }

    /**
     * Creates an endpoint '/companies/save', at which the company from the request body is saved to the database
     *
     * @param toSave The company to be saved to the database
     */
    @PostMapping(value = "/companies/save",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void saveCompany(@RequestBody Company toSave) {
        service.saveEntity(toSave);
    }

    /**
     * Creates an endpoint '/companies/update', at which the company from the request body is updated in the database
     *
     * @param toUpdate The company to be updated in the database
     */
    @PutMapping(value = "/companies/update",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateCompany(@RequestBody Company toUpdate) {
        service.updateEntity(toUpdate);
    }

    /**
     * Creates an endpoint '/companies/delete', at which the company, referred by the id, is deleted from the database
     *
     * @param id The id of the company to be deleted from the database
     */
    @DeleteMapping(value = "/companies/delete")
    public void deleteCompany(@RequestParam int id) {
        service.deleteEntity(id);
    }
}
