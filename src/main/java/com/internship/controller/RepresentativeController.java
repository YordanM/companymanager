package com.internship.controller;

import com.internship.pojo.Representative;
import com.internship.service.IService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Yordan Marinov
 */
@RestController
public class RepresentativeController {
    private IService<Representative> service;


    public RepresentativeController(IService<Representative> service) {
        this.service = service;
    }

    /**
     * Creates an endpoint '/representatives', at which all of the representatives are displayed
     *
     * @return Returns a list of representatives, retrieved from the database
     */
    @GetMapping(value = "/representatives")
    public List<Representative> getAll() {
        return service.getAllEntities();
    }

    /**
     * Creates an endpoint '/representatives/get', at which the representatives, referred by the id, is displayed
     *
     * @param id The id of the representatives to be fetched from the database
     * @return Returns the representatives which has the specified id, or null if no representatives with that id exists
     */
    @GetMapping(value = "/representatives/get")
    public Representative getRepresentative(@RequestParam int id) {
        return service.getEntityById(id);
    }

    /**
     * Creates an endpoint '/representatives/save', at which the representatives from the request body is saved to the database
     *
     * @param toSave The representatives to be saved to the database
     */
    @PostMapping(value = "/representatives/save",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void saveRepresentative(@RequestBody Representative toSave) {
        service.saveEntity(toSave);
    }

    /**
     * Creates an endpoint '/representatives/update', at which the representatives from the request body is updated in the database
     *
     * @param toUpdate The representatives to be updated in the database
     */
    @PutMapping(value = "/representatives/update",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateRepresentative(@RequestBody Representative toUpdate)
    {
        service.updateEntity(toUpdate);
    }

    /**
     * Creates an endpoint '/representatives/delete', at which the representatives, referred by the id, is deleted from the database
     *
     * @param id The id of the representatives to be deleted from the database
     */
    @DeleteMapping(value = "/representatives/delete")
    public void deleteRepresentative(@RequestParam int id)
    {
        service.deleteEntity(id);
    }
}
